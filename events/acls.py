from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests


def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "per_page": 1,
        "query": city + " " + state,
    }
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}


# ORIGINAL GET_PHOTO #
# def get_photo(city, state):
#     headers = {"Authorization": PEXELS_API_KEY}
#     url = (
#         "https://api.pexels.com/v1/search?query="
#         + str(city)
#         + ","
#         + str(state)
#     )
#     # q_string = str(city) + ", " + str(state)
#     r = requests.get(
#         url=url,
#         headers=headers,
#     )
#     photo = json.loads(r.content)
#     photo_url = {
#         "photo_url": photo["photos"][0]["url"],
#     }
#     return photo_url
# ORIGINAL GET_PHOTO END #


def get_weather(city, state):
    params = {
        "q": f"{city}, {state}, US",
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY,
    }
    url = "http://api.openweathermap.org/geo/1.0/direct"
    response = requests.get(url, params=params)
    content = json.loads(response.content)

    try:
        latitude = content[0]["lat"]
        longitude = content[0]["lon"]
    except (KeyError, IndexError):
        return None

    params = {
        "lat": latitude,
        "lon": longitude,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }

    url = "http://api.openweathermap.org/data/2.5/weather"
    response = requests.get(url, params=params)
    content = json.loads(response.content)

    try:
        return {
            "description": content["weather"][0]["description"],
            "temp": content["main"]["temp"],
        }
    except (KeyError, IndexError):
        return None


# ORIGINAL GET_WEATHER_DATA #
# def get_weather_data(city, state):
#     headers = {"Authorization": OPEN_WEATHER_API_KEY}
#     url = (
#         "http://api.openweathermap.org/geo/1.0/direct?q="
#         + str(city)
#         + ","
#         + str(state)
#         + "840"
#         + "&appid="
#         + OPEN_WEATHER_API_KEY
#     )
#     r = requests.get(
#         url=url,
#         headers=headers,
#     )
#     weather = json.loads(r.content)
#     lat = weather[0]["lat"]
#     lon = weather[0]["lon"]

#     url = (
#         "https://api.openweathermap.org/data/2.5/weather?lat="
#         + str(lat)
#         + "&lon="
#         + str(lon)
#         + "&appid="
#         + OPEN_WEATHER_API_KEY
#     )
#     r = requests.get(url=url, headers=headers)
#     weather_data = json.loads(r.content)

#     temp = weather_data["main"]["temp"]
#     description = weather_data["weather"][0]["description"]

#     d = {
#         "temp": temp,
#         "description": description,
#     }
#     return d
# ORIGINAL GET_WEATHER_DATA END#
